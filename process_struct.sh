#!/bin/bash
#
# Author: Fidel Alfaro Almagro
# 20-Nov-2021 8:45
# Version $1.0
#

# General info
echo "HOST INFO: `uname -a`"
echo "Starting time: "`date`
START=$(date +%s)

# Variable definition
subj="$1"
anatDir=$(find ${subj}  -type d -name "anat"   | head -n 1)
T1w=$(find ${anatDir}   -name "*T1w*nii.gz"    | tail -n 1)
FLAIR=$(find ${anatDir} -name "*FLAIRw*nii.gz" | tail -n 1)
outputDir="${subj}/derivatives"

# Checks for input files
if [ ! -f ${T1w} ] ; then
    echo "No proper T1w image"
    exit
fi

if [ ! -f ${FLAIR}} ] ; then
    optFLAiR="";
else
    optFLAIR="-FLAIR ${FLAIR} -FLAIRpial"
fi

# Create output directories
mkdir -p ${outputDir}/FSL_anat/
mkdir -p ${outputDir}/FreeSurfer/

#####################
# Actual processing #
#####################
${FSLDIR}/bin/fsl_anat -i ${T1w} -o ${outputDir}/FSL_anat/ --nocleanup
export SUBJECTS_DIR="${outputDir}/FreeSurfer/"
recon-all -subjid ${subj} -i ${T1w} $optFLAIR

# House-keeping
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "The command $PARENT_COMMAND took $DIFF seconds"
echo "Ending time: "`date`
